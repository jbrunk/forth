0 variable x1
0 variable y1
0 variable x2
0 variable y2
0 variable dx
0 variable dy
0 variable ox
0 variable oy
0 variable steps
0 variable doPlot

: route ( y1 x1 y2 x2 -- yNew xNew)
x2 ! y2 ! x1 ! y1 !

doPlot @ 1 = if
    ( y1 @ x1 @ at ." F" )
    ( y2 @ x2 @ at ." *" )
    x2 @ y2 @ 1 plot
then

y1 @ y2 @ - dy !
x1 @ x2 @ - dx !
dy @ abs dx @ abs + steps !

doPlot @ 0 = if
    ." y1=" y1 @ . ." x1=" x1 @ . ." y2=" y2 @ . ." x2=" x2 @ . ." dy=" dy @ . ." dx=" dx @ . ." steps=" steps @ . CR
then

steps @ 0= if y1 @ x1 @ exit then

dy @ 0 < if 1 oy ! then
dy @ 0 > if -1 oy ! then
dy @ 0 = if 0 oy ! then

dx @ 0 < if 1 ox ! then
dx @ 0 > if -1 ox ! then
dx @ 0 = if 0 ox ! then

y1 @ x1 @ y2 @ oy @ - x2 @ ox @ - route
;

