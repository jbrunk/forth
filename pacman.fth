: star 42 emit ;

: stars 0 do star loop ;

: hex 16 base c! ;

: bin 2 base c! ;

: oct 8 base c! ;

definer code does> call ;

hex code rnd ed c, 5f c, 16 c, 00 c, 5f c, d7 c, fd c, e9 c,  decimal

hex code clock 76 c, fd c, e9 c, decimal

: wait 0 do clock loop ;

11264 constant charset
9216 constant videoram
15403 constant frames

: udgs ( byte1..8 <char> -- , user defined graphic set )
8 * charset + dup 8 +
do
    i 1- c!
-1 +loop
;

bin
00000000
00111100
01111110
01111110
01111110
01111110
00111100
00000000
decimal
3 udgs

bin
00000000
00111100
01111110
01111000
01111000
01111110
00111100
00000000
decimal
4 udgs

bin
00000000
00111110
01111100
01110000
01110000
01111100
00111110
00000000
decimal
5 udgs

bin
00000000
00000000
00000000
00000000
00001000
00000000
00000000
00000000
decimal
6 udgs

6  constant dot
32 constant blank

bin
00111100
01000010
10000001
10100101
10000001
10000001
10111101
11011011
decimal
7 udgs

bin
00111100
01000010
10000001
10100101
10000001
10000001
10011001
01100110
decimal
8 udgs

bin
00111100
01111110
11111111
11011011
11111111
11111111
10111101
11011011
decimal
9 udgs

bin
00111100
01111110
11111111
11011011
11111111
11111111
10011001
01100110
decimal
10 udgs

bin
00000000
00010000
00111000
01111100
00111000
00010000
00000000
00000000
decimal
11 udgs
11 constant gem

bin
00000000
01000010
11100111
01000010
00000000
01111110
00001100
00001100
decimal
12 udgs

bin
00000000
01010100
00101010
01010100
00101010
01010100
00101010
00000000
decimal
14 udgs
14 constant brick

0 variable Points
0 variable pacmanIndex
0 variable ghost1Index
0 variable ghost1curX
0 variable ghost1curY
5 constant ghostWait
ghostWait variable ghost1Wait
0 variable ghost2Index
0 variable ghost3Index
0 variable ghost4Index
0 variable oldX
0 variable oldY
0 variable oldChar
0 variable curX
0 variable curY
24 constant maxX
21 constant maxY
174 constant maxDots
0 variable Dots
3 constant maxLifes
maxLifes variable Lifes

0 variable x1
0 variable y1
0 variable x2
0 variable y2
0 variable dx
0 variable dy
0 variable ox
0 variable oy
0 variable steps

: routeGhost ( y1 x1 y2 x2 -- yNew xNew )
x2 ! y2 ! x1 ! y1 !

y1 @ y2 @ - dy !
x1 @ x2 @ - dx !
dy @ abs dx @ abs + steps !

steps @ 0 = if x1 @ y1 @ exit then

dy @ 0 < if 1 oy ! then
dy @ 0 > if -1 oy ! then
dy @ 0 = if 0 oy ! then

dx @ 0 < if 1 ox ! then
dx @ 0 > if -1 ox ! then
dx @ 0 = if 0 ox ! then

x2 @ ox @ - y2 @ oy @ -
;

: drawGhost1
ghost1Wait @ 0= if ghostWait ghost1Wait ! else ghost1Wait @ 1- ghost1Wait ! exit then

curY @ curX @ ghost1curY @ ghost1curX @ routeGhost ghost1curY ! ghost1curX !
ghost1curY @ ghost1curX @ at

ghost1Index @ 0 = if 7 emit then
ghost1Index @ 1 = if 8 emit then
ghost1Index @ 1+ 2 /mod drop ghost1Index !
;

: drawGhost2
10 13 at
ghost2Index @ 0 = if 8 emit then
ghost2Index @ 1 = if 7 emit then
ghost2Index @ 1+ 2 /mod drop ghost2Index !
;

: drawGhost3
10 12 at
ghost3Index @ 0 = if 9 emit then
ghost3Index @ 1 = if 10 emit then
ghost3Index @ 1+ 2 /mod drop ghost3Index !
;

: drawGhost4
10 15 at
ghost4Index @ 0 = if 10 emit then
ghost4Index @ 1 = if 9 emit then
ghost4Index @ 1+ 2 /mod drop ghost4Index !
;

: drawGhosts
drawGhost1 drawGhost2 drawGhost3
;

: get ( y x -- c@ from videoram )
swap 32 * videoram + + c@
;

: allEatenUp
    0 0 at 32 spaces 0 0 at ." ### YOU WON ###"
    abort
;

: Points++
curY @ curX @ get
dup dot = if Points @ 1+ Points ! Dots @ 1+ Dots ! then
dup gem = if Points @ 50 + Points ! then
drop
Dots @ maxDots = if allEatenUp then
;

: GhostCollision? 
curY @ curX @ get
dup dup 6 > swap 11 < and if 0 0 at 32 spaces 0 0 at ." GAME OVER!" curY @ curX @ at 12 emit leave then
drop
;

: drawPacman
oldY @ oldX @ get oldChar c!
oldY @ oldX @ at space
curY @ curX @ at
pacmanIndex @ 0 = if 3 emit then
pacmanIndex @ 1 = if 4 emit then
pacmanIndex @ 2 = if 5 emit then
pacmanIndex @ 3 = if 4 emit then
curX @ oldX !
curY @ oldY !
pacmanIndex @ 1+ 4 /mod drop pacmanIndex !
;

: statusLine
0 0 at ." Points:" Points @ . ." X:" curX @ . ." Y:" curY @ . ." Dots:" Dots @ . 2 spaces
;

: hbricks ( row column n -- , draw ascii line )
rot rot at
0 do
  brick emit
loop
;

: vbricks ( row column n --, draw ascii line )
0 do
  over over swap i +
  swap at brick emit
loop
drop drop
;

: drawField
maxY 1+ 1 do
	maxX 1+ 0 do
		j i at dot emit
	loop
loop

1 0 maxX 1+ hbricks
maxY 0 maxX 1+ hbricks
1 0 maxY vbricks
1 maxX maxY vbricks

8 9 7 hbricks
12 9 7 hbricks
8 9 5 vbricks
8 15 5 vbricks

3 2 4 hbricks
4 2 4 hbricks
5 2 4 hbricks
6 2 4 hbricks

3 7 4 hbricks
4 7 4 hbricks
5 7 4 hbricks
6 7 4 hbricks

1 12 6 vbricks
15 12 2 vbricks
18 12 3 vbricks

3 14 4 hbricks
4 14 4 hbricks
5 14 4 hbricks
6 14 4 hbricks

3 19 4 hbricks
4 19 4 hbricks
5 19 4 hbricks
6 19 4 hbricks

8 0 6 hbricks
9 0 6 hbricks
10 0 6 hbricks
12 0 6 hbricks
13 0 6 hbricks
14 0 6 hbricks
11 0 at 6 spaces

8 19 6 hbricks
9 19 6 hbricks
10 19 6 hbricks
12 19 6 hbricks
13 19 6 hbricks
14 19 6 hbricks
11 maxX 5 - at 6 spaces

8 7 9 vbricks
11 7 at dot emit
8 17 9 vbricks
11 17 at dot emit
14 9 7 hbricks

16 2 4 hbricks
17 2 4 hbricks
18 2 4 hbricks
19 2 4 hbricks

16 19 4 hbricks
17 19 4 hbricks
18 19 4 hbricks
19 19 4 hbricks

18 7 4 hbricks
19 7 4 hbricks
18 14 4 hbricks
19 14 4 hbricks
16 9 4 vbricks
16 10 4 vbricks
16 14 4 vbricks
16 15 4 vbricks

2 1 at gem emit
2 maxX 1- at gem emit
20 1 at gem emit
20 maxX 1- at gem emit

13 curY !  12 curX !
10 Ghost1curY ! 11 Ghost1curX !

;

0 variable newX
0 variable newY

: keys?
curX @ newX !
curY @ newY !
inkey
dup 27 = if leave then 
dup ascii q = if leave then 
dup 1 = if curX @ 1 - dup 1+ 0 > if newX ! else drop maxX newX ! then then 
dup 3 = if curX @ 1 + dup 1- maxX < if newX ! else drop 0 newX ! then then
dup 7 = if curY @ 1 - dup 0 > if newY ! else drop then then
dup 9 = if curY @ 1 + dup 1- maxY < if newY ! else drop then then
drop
newY @ newX @ get brick = if else newX @ curX ! newY @ curY ! then
;

: pacman
invis fast cls drawField
0 Points !
0 Dots !
13 curY !
12 curX !
begin
	statusLine
	drawPacman
	250 wait
	drawGhosts
    GhostCollision?
	keys?
	Points++
	250 wait
0 until
;

: ULIST
CLS 15431 @ 0 15431 !
VLIST DROP 15431 !
;

: .S
15419 @ HERE 12 +
OVER OVER -
IF
  DO
    I @ . 2
  +LOOP
ELSE
  DROP DROP
THEN
CR
;

: depth
15419 @ HERE 12 +
- 2 /
;

15403 @ variable seed
: RND ( n -- 0..n-1, random number )
Seed @ 75 u* 75 0 D+ 
OVER OVER u< - - 1-
DUP Seed !
u* SWAP DROP
;

redefine rnd

